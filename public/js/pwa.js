'use strict';
sky_log('** PROGRESSIVE WEB APPLICATION *** ', '** WRITE ALWAYS ***');

// Auto Hide SEO Containers
window.addEventListener('DOMContentLoaded', function () {
  $("#rock_content_article").toggle();
});

var db = new Dexie("bento-data-store");
db.version(1).stores({
  api_data: '++id'
});

let nodeID = $("#hidden_document_id").val() ? $("#hidden_document_id").val() : 1;

$(function () {
  // Fire Sky Worker To Load Data In Background
  const fireSkyWorker = (row) => {
    row.each(function() {
      //var node_status = ($(this).html().indexOf('Published') == 0) ? 'success' : 'info';
      let rockWorker = new Worker("/js/sky_worker_doc.js");
      rockWorker.postMessage({
        action: "",
        msg: "Sky Worker, Please Fetch Data",
        url: "",
        nid: $(this).text()
      });
    });
  }

  const populateOnForm = (data) => {
    // Add data_field = "seo_headline" to input field
    // sky_log("PWA CMS", data);
    $(".sky_auto_save").each(function () {
      let fieldValue = $(this).attr("data_field");
      if (fieldValue) {
        $(this).val(data.nodeObject.entry[fieldValue]);
      }
    });

    // Handle Complex Fields.
    let mainART = data.image.includes(".fit-760w") ? data.image : aimsify(data.image, ".fit-760w");
    if (mainART == "") mainART = "/assets/no_image_main_art.jpg";
    $(".main_art_image").attr("src", mainART);
    $("#rock_cms_header").html(data.nodeObject.entry.title);
    $("#tinymce-content-body").html(data.nodeObject.entry.content);
  }

  const fetchLocalData = (nodeID) => {
    var collection = db.api_data.where("id").equalsIgnoreCase(nodeID);
    collection.each(function (data) {
      populateOnForm(data);
    });
  }

  
  $("#rock_dash_list").DataTable({
    ajax: { url: "/api/dash/dashdata", method: "GET", cache: true },
    select: true,
    pageLength: 25,
    stateSave: true,
    deferRender: true,
    columns: [
      { data: "nid" },
      { data: "image" },
      { data: "title" },
      { data: "type" },
      { data: "user" }
    ],
    "oLanguage": {

      "sSearch": "Search (Last 500 Records):"

    },
    initComplete: function (settings, json) {
      $("#rock_dash_list td a").click(function (event) {
        event.preventDefault();
        nodeID = $(this).text();
        rock_current_post_id = nodeID;

        //Place Selected NID for all other future actions
        $("#rock_cms_nid").val(rock_current_post_id);

        $("#rock_content_article").toggle();
        $("#rock_content_dashbaord").toggle();
        

        if ($("#rock_content_article").is(":visible")) {
          $(".publish_btn_group").show();
          $("#post_right_content").show();
          $("#dash_right_content").hide();
        }

        fetchLocalData(nodeID);
      });
    }
  }).on('draw', function () {
    fireSkyWorker($("tr td:nth-child(1) a"));
  });


  $("#post_right_content").hide();
  $(".publish_btn_group").hide();


  $(".pwa_dash_board").click(function (event) {
    $(".main_art_image").attr("src", "/assets/no_image_main_art.jpg");
    $("#rock_content_dashbaord").show();
    $("#rock_content_article").hide();
    $(".publish_btn_group").hide();
    $("#post_right_content").hide();
    $("#dash_right_content").show();
  });
});


async function saveLocalData() {
  let result = await db.api_data.where("id").equals(nodeID).first();
  console.log("RRRRRR ", result);
  $(".sky_auto_save").each(function() {
    let fieldValue = $(this).attr("data_field");
    if (fieldValue) {
      result.nodeObject.entry[fieldValue] = $(this).val();
    }
  });
  result.nodeObject.entry.title = $("#rock_cms_header").html();
  result.nodeObject.entry.content = $("#tinymce-content-body").html();
  sky_log("IMAGE PROB", $(".main_art_image").attr("src"));
  await db.api_data.put({
    id: nodeID.toString(),
    image: $(".main_art_image").attr("src").toString(),
    nodeObject: result.nodeObject
  });
}

$("#main_art_top").click(function() {
  alert("Handler for .click() called.");
  // Update Local Storage Copy On CTA
  saveLocalData();
});

$(".sky_auto_save").keydown(function(event) {
  saveLocalData();
});

$(".btn_breaking_news, .btn_create_new").click(function(event) {
  event.preventDefault();
  rock_current_post_id = 0;
  //Place Selected NID for all other future actions
  $("#rock_cms_nid").val(rock_current_post_id);
  $("#rock_content_dashbaord").hide();
  $("#rock_content_article").show();
  $(".publish_btn_group").show();
  $("#post_right_content").show();
  $("#dash_right_content").hide();
  $(".btn_breaking_news").hide();
  $("#rock_cms_header").html("Enter Title....");
  $("#tinymce-content-body").html("Enter Content....");
  $(".main_art_image").attr("src", "/assets/no_image_main_art.jpg");  
  if ($(this).attr('name') == 'btn_breaking_news') {
    $("#breaking_news").val(1);
  } else {
    $("#breaking_news").val(0);
  }
});