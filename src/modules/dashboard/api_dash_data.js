
import * as form from "../../lib/form_api";
import * as skyC from "../../lib/sky_connect.js";
import fs from "fs";

module.exports = async function (req, res) {
  const currentTime = Math.floor(Date.now() / 1000);
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
  res.header('Expires', '-1');
  res.header('Pragma', 'no-cache');

  logger("Loading Dashboard API Data");

  let dashData = await skyC.list();
  // Use Failover Endpoint If DB Is Not Reachable.
  if (dashData == false) {
    logger("DB Is Not Reachable!!!. Using Failover Strategy.", 'error');
    dashData = JSON.parse(fs.readFileSync("././public/data/dash.json", "utf8"));
    res.json({ data: dashData.data });
  } else {

    let listData = {};
    let tableData = [];
    let i = 0;
    let nid;
    for (var data in dashData) {
      if (dashData.hasOwnProperty(data)) {
        listData = {};
        //console.log(dashData[data]);
        nid = dashData[data].nid;
        listData.nid = `<a href = "/doc/${nid}">${nid}</a>`;
        listData.image = `<a href = "/doc/${nid}"><img src = "${dashData[data].thumb}" class = "img-thumbnail dash_thumb" target = "_blank"></a>`;
        listData.title = `<h6>${dashData[data].entry.title} <span class="badge badge-warning">New</span></h6>`;
        listData.type = dashData[data].type;
        listData.user = dashData[data].user;
        tableData[i] = listData;
        //console.log(tableData);
        i++;
      }
    }
      res.json({ data: tableData });
  }

};
