# Rock CMS PWA [![coverage report](https://gitlab.com/nycdidar/rock-cms-pwa/badges/master/coverage.svg)](https://gitlab.com/nycdidar/rock-cms-pwa/commits/master)

Rock CMS PWA
Progressive CMS Application

npm start
Visit localhost:3000/login
Enter email address to login

Offline action kicks in after reloading the dashboard once or twice for service worker to cache all files. (this can be optimized later).



