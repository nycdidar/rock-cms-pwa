
const config = {};

config.db_settings = {
  host: process.env.DATABASE_HOST || "mysql",
  user: "bdp_bdpage",
  password: "eastcedar",
  database: "rockcms",
  port: "3307"
};

config.mongo_conn_str = "mongodb+srv://didar911:SkyBird@rock-cluster-tuyng.mongodb.net/test?retryWrites=true";
config.mongo_db = "rock-cms";

config.port = process.env.PORT || 3000;
config.bento_api_port = process.env.PORT || 4000;

config.rabbit_prod = "amqp://nbcndqanewscms:qxNbOV*US7Yj1rZc@ppe-rabbit.nbcnewstools.net:8080";
//config.rabbit_prod = "amqp://guest:guest@localhost:5672";
config.rabbit_type = "direct";
config.rabbit_exchange = "newscms_qa_content";
config.rabbit_file_exchange = "newscms_qa_file";
config.rabbit_queue = "test";
//config.rabbit_exchange = "todaycmscontent";

config.rabbit_key = 'live';

config.asset_paths = {
  "assets": "assets",
  "views": "views",
  "public": "./public",
  "node_modules": "node_modules"
};

config.queryRecLen = 20;
config.local = false;

config.bentoApiEndpoint = 'https://staging.newsdigitalapi.com';
config.msgSaverEndpoint = "https://kemne.com/didar/cwhendpoint.php";

module.exports = config;
