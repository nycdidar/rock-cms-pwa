/**
 * SKY HELPER FUNCTIONS
 */
//--- Global App Variable ------
window.rock_current_post_id = 0;
window.rock_current_user_id = 0;
window.rock_current_user_email = "";
//------------------------------

window.sky_log = function (msg, obj) {
  var styles = [
    'background: linear-gradient(#D33106, #571402)'
    , 'border: 1px solid #3E0E02'
    , 'color: white'
    , 'text-shadow: 0 1px 0 rgba(0, 0, 0, 0.3)'
    , 'box-shadow: 0 1px 0 rgba(255, 255, 255, 0.4) inset, 0 5px 3px -5px rgba(0, 0, 0, 0.5), 0 -13px 5px -10px rgba(255, 255, 255, 0.4) inset'
    , 'line-height: 18px'
    , 'text-align: center'
    , 'font-weight: bold'
    , 'font-family:"Helvetica Neue", Helevetica; font-size:10px;'
  ].join(';');
  msg = (arguments.length === 2) ? msg + ' ' : msg;
  obj = (obj) ? obj : '';
  if (typeof msg !== 'string') {
    console.log('%c [ Rock CMS ] ' + 'data ', styles, msg);
  } else {
    console.log("%c [ Rock CMS ] " + msg, styles, obj);
  }
}