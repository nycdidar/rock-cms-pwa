/**
 * Rock CMS Router
 */
import errorHandler from "./error_handler";

module.exports = function (app) {

  logger(`Router Access`, "info");

  app.all("/system/:jsfile", require("../modules/system/system"), errorHandler);

  app.all("/media", require("../modules/media/media"), errorHandler);

  app.all("/", require("../modules/login/login"), errorHandler);
  app.all("/login", require("../modules/login/login"), errorHandler);

  app.all("/search/:keyword", require("../modules/search/search"), errorHandler);
  
  app.all("/api/dash/:dashData?", require("../modules/dashboard/api_dash_data"), errorHandler);

  app.all("/post/:postID?", require("../modules/pwa/pwa"), errorHandler);

  app.all("/submit-article-data", require("../modules/rabbit_post/rabbit_post"), errorHandler);

  app.all("/aimsify-image", require("../modules/image_post/image_post"), errorHandler);

  // Catch unmatched routes
  app.all("*", (req, res) => {
    logger(`URL Request: ${req.params[0]}`, "info");
    res.json({ status: 404, message: "Page not found." });
  });

};
