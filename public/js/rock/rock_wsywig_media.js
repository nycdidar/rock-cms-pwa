
tinymce.PluginManager.add('rock_media', function (editor, url) {
  // Add a button that opens a window

  editor.addMenuItem("rock_media", {
    text: "Insert Some Content",
    context: "tools",
    onclick: function () {
      $('#rock_media_modal').modal('show');
      //editor.insertContent("Here's some content!");
    }
  });

  editor.addButton("rock_media", {
    text: "Media",
    icon: false,
    onclick: function() {
      $('#rock_media_modal').modal('show'); 
    }
  });

  return {
    getMetadata: function () {
      return {
        name: "Rock CMS Media Plugin",
        url: "http://nbcnews.com"
      };
    }
  };
});
