// Form API
import fs from "fs";
import * as utils from "./utility";

const constraints = {};
let global_js_header = "";
let common_js = "";
let validation = false;

const addItem = (field, element) => {
  constraints[field] = element;
};

export function resetHeader() {
  global_js_header = '';
  return global_js_header;
}

const validationOutput = () => {
  return constraints;
}

export function formJSHeader() {
  return global_js_header;
}

export function commonJS() {
  common_js = `
      
      
      `;
  return (validation) ? common_js : '';
}

export function inputTextAreaField (params) {
  const fieldName = params.name ? params.name : utils.slugify(params.title);
  const fieldID = params.id ? params.id : utils.slugify(params.title);
  const fieldPlaceholder = params.placeholder ? params.placeholder : "Enter ...";
  const fieldHelpText = params.helpText ? `<div class="valid-feedback" > ${params.helpText}</div>` : "";
  const fieldValue = params.value ? params.value : "";
  let ValidationPattern,
    DataError,
    RequiredLabel = "";
  if (params.validation !== false) {
    validation = true;
    addItem(fieldName, params.validation);
    RequiredLabel = '<span class="form-required" title="This field is required.">*</span>';
  }

  return `
<div class="form-group">
  <label>${params.title} ${RequiredLabel} </label>
  <textarea class="form-control ${params.class}" rows="3" id = "${fieldID}" name = "${fieldName}" placeholder="${fieldPlaceholder}">${fieldValue}</textarea>
  ${fieldHelpText}
</div>
`;
}

export function inputTextField (params) {
  let multiple = "";
  let max = "";
  let addMore = "";
  const fieldName = params.name ? params.name : utils.slugify(params.title);
  const fieldID = params.id ? params.id : utils.slugify(params.title);
  const fieldClass = params.class ? params.class : utils.slugify(params.class);
  const fieldValue = params.value ? params.value : "";
  const fieldHelpText = params.helpText ? `<div class="valid-feedback" > ${params.helpText}</div>` : "";

  let ValidationPattern,
    DataError,
    RequiredLabel = "";
  

  
  if (params.validation !== false) {
    validation = true;
    addItem(fieldName, params.validation);
    RequiredLabel = '<span class="form-required" title="This field is required.">*</span>';
  }
  const labelDOM = params.label ? `<label>${params.label} ${RequiredLabel}</label>` : "";

  if (params.ajax) {
    global_js_header += `
    $("#${params.id}").autocomplete({
      source: function(request, response) {
        $.ajax( {
          url: '${params.remote_url}',
          data: {
            term: request.term
          },
          beforeSend: function(xhr) {
          },
          success: function(data) {
            response(data);
          }
        } );
      },
      minLength: 1,
      select: function( event, ui ) {
        $("#${params.id}").val(ui.item.value);
        //$("#hidden_${params.id}").val(ui.item.node_id);
        return false;
      }
    });
        `;
  }


  if (params.multi) {
    global_js_header += `
      var availableTags = [
        "ActionScript",
        "AppleScript",
        "Asp",
        "BASIC",
        "C",
        "C++",
        "Clojure",
        "COBOL",
        "ColdFusion",
        "Erlang",
        "Fortran",
        "Groovy",
        "Haskell",
        "Java",
        "JavaScript",
        "Lisp",
        "Perl",
        "PHP",
        "Python",
        "Ruby",
        "Scala",
        "Scheme"
      ];
        split = (val) => {
        return val.split(/,\s*/);
      }
        extractLast = (term) => {
        return split(term).pop();
      }

    $("#${params.id}")
      // don't navigate away from the field on tab when selecting an item
      .on("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
          $(this).autocomplete("instance").menu.active) {
          event.preventDefault();
        }
      })
      .autocomplete({
        minLength: 0,
        source: function (request, response) {
          // delegate back to autocomplete, but extract the last term
          response($.ui.autocomplete.filter(
            availableTags, extractLast(request.term)));
        },
        focus: function () {
          // prevent value inserted on focus
          return false;
        },
        select: function (event, ui) {
          var terms = split(this.value);
          // remove the current input
          terms.pop();
          // add the selected item
          terms.push(ui.item.value);
          // add placeholder to get the comma-and-space at the end
          terms.push("");
          this.value = terms.join(", ");
          return false;
        }
      });
`;
  }

  return `
<div class="form-group ${fieldClass}">
  ${labelDOM}
  <input class="form-control ${fieldClass}" id = "${fieldID}" name = "${fieldName}" value = "${fieldValue}">
  ${fieldHelpText}
</div>
`;
}


export function inputCheckBoxField (params) {
  const fieldName = params.name ? params.name : utils.slugify(params.title);
  const fieldID = params.id ? params.id : utils.slugify(params.title);
  const fieldValue = params.value ? "checked" : "";
  const fieldHelpText = params.helpText ? `<div class="valid-feedback" > ${params.helpText}</div>` : "";

  let ValidationPattern,
    DataError,
    RequiredLabel = "";

  if (params.validation !== false) {
    validation = true;
    addItem(fieldName, params.validation);
    RequiredLabel = '<span class="form-required" title="This field is required.">*</span>';
  }

  return `
  <div class="checkbox ${params.class} ">
    <label>
      <input type="checkbox" title="${fieldID}" id="${fieldID}" name="${fieldName}" value="1" ${fieldValue}>
      ${params.title} ${RequiredLabel}
    </label>
    <div class="messages"></div>
    ${fieldHelpText}
  </div>
`;
}


export function inputRadioField(params) {
  const fieldName = params.name ? params.name : utils.slugify(params.title);
  const fieldID = params.id ? params.id : utils.slugify(params.title);
  const fieldValue = params.value ? "checked" : "";
  const fieldHelpText = params.helpText ? `<div class="valid-feedback" > ${params.helpText}</div>` : "";
  let ValidationPattern,
    DataError,
    RequiredLabel = "";
  if (params.validation !== false) {
    validation = true;
    addItem(fieldName, params.validation);
    RequiredLabel = '<span class="form-required" title="This field is required.">*</span>';
  }

  return `
<div class="form-group">
  <div class="radio">
    <label>
      <input type="radio" class="${params.class}" id = "${fieldID}" name = "${fieldName}" value = "1" ${fieldValue}>
      ${params.title} ${RequiredLabel}
    </label>
     ${fieldHelpText}
  </div>
</div>
`;
}

export function inputDateField(params) {
  const fieldName = params.name ? params.name : utils.slugify(params.title);
  const fieldID = params.id ? params.id : utils.slugify(params.title);
  const fieldHelpText = params.helpText ? `<div class="valid-feedback" > ${params.helpText}</div>` : "";
  return `
      <div class="form-group ${params.class}">
        <label>${params.title}</label>
        <div class="input-group">
          <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
          </div>
          <input type="text" name = "${fieldName}" value="${params.value}" title="${params.title}" class="form-control pull-right publishing-timestamp" id="${fieldID}">
          ${fieldHelpText}
          </div>
      </div>`;
}


export function inputListField(params) {
  const fieldName = params.name ? params.name : utils.slugify(params.title);
  const fieldID = params.id ? params.id : utils.slugify(params.title);
  const fieldHelpText = params.helpText ? `<div class="valid-feedback" > ${params.helpText}</div>` : "";
  let ValidationPattern,
    DataError,
    RequiredLabel = "";
  if (params.validation !== false) {
    validation = true;
    addItem(fieldName, params.validation);
    RequiredLabel = '<span class="form-required" title="This field is required.">*</span>';
  }

  const multiSelect = params.multiselect ? ' multiple="multiple"' : "";

  // console.log(typeof params.list_values);
  let listValuesDOM = "";
  if (typeof params.list_values == "string") {
    params.list_values = JSON.parse(fs.readFileSync(params.list_values, "utf8"));
  }

  Object.keys(params.list_values).forEach(key => {
    let rowID = typeof params.list_values[key] == "object" ? params.list_values[key].id : key;
    let rowValue = (typeof params.list_values[key] == "object") ? params.list_values[key].value : params.list_values[key];
    const selectedVal = params.value === key ? "selected" : "";
    listValuesDOM += `<option title="${rowValue}" value="${rowID}" ${selectedVal}>${rowValue}</option>`;
  });

  return `
<div class="form-group ${params.class}">
  <label>${params.title} ${RequiredLabel}</label>
  <select class="form-control select2 ${params.class}" id = "${fieldID}" name = "${fieldName}" ${multiSelect}>` + listValuesDOM + `
  </select>
  ${fieldHelpText}
</div>`;
}

export function displayMSG(params) {
  return `
            <div class="alert alert-primary top-header-notification" role="alert" id="top-header-notification">
              ${params.msg}
            </div>`;
}
