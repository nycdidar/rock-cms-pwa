import * as skyC from "../../lib/sky_connect.js";
import * as utility from "../../lib/utility";
import * as fetch from  "node-fetch";
module.exports = async function (req, res) {
  const currentTime = Math.floor(Date.now() / 1000);
  logger("M at AIMSIFY");
  // logger(util.generateUID().fid);

  const postID = req.params.postID ? req.params.postID : false;
  let sessionObj = (req.session.userData) ? req.session.userData : {};



  
  let imageData = {
    caption: 'This is a Dynamically Inserted Image',
    imageURL: 'http://kemne.com/didar/test.jpg',
    filename: 'test.jpg',
    user: "didarul.amin@nbcuni.com",
    uid: 4806
  }
  let aimsData = utility.generateAIMS(imageData);

  const fetchImageInfo = () => {
    fetch(`https://uat2-msgviewer.nbcnewstools.net/msgsaver/nbcnews/image/${aimsData.entry.fid}`)
      .then(res => res.json())
      .then(json => {
        console.log("AIMS Path: ", json[0].entry.aims_image_path);
        res.send({
          status: 200,
          msg: "Sent Successfully",
          fid: aimsData.entry.fid,
          imageURL: json[0].entry.aims_image_path
        });
      });
  };


  skyC.send(aimsData, "file", fetchImageInfo);
  //console.log("NODE ID: ", JSON.stringify(utility.generateAIMS(imageData)));
  console.log("Image FID: ", aimsData.entry.fid);

};
