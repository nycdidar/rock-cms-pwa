import express from "express";
import path from "path";
import request from "request";
import fs from "fs";
import config from "./config";
import session from "express-session";
import fileUpload from "express-fileupload";
import logger from "./boot/logger";

require("dotenv").config();
const router = express.Router();
const app = express();

global.rock_root = __dirname;

global.logger = (msg = "", type = "info") => {
  return logger[type](msg);
};

const https = require("https"),
  helmet = require("helmet");
app.use(helmet());

app.use(express.static(__dirname, { dotfiles: 'allow' }));
app.use(fileUpload());
app.set('view engine', 'ejs');
app.set("views", path.join(__dirname, "views"));
app.use(session({ secret: 'rock-liste-secret-token', cookie: { maxAge: 6000000 } }));

// Allow NODE to expose/route the paths
for (var key in config.asset_paths) {
  if (config.asset_paths.hasOwnProperty(key)) {
    app.use(express.static(config.asset_paths[key]));
  }
}

let bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

import routeModule from "./boot/routes";
routeModule(app);

const http = require('http').Server(app),
  io = require('socket.io')(http);
var socketPort = process.env.SOCKET_PORT || 3001;

io.on('connection', (socket) => {
  console.log('Socket: New Client Connected');

  socket.on('rock_comm', (incomngMSG) => {
    console.log('rock_comm to: ', incomngMSG);
    socket.emit('rock_comm', 'Socket Server: I am sending you a message');
  });

  socket.on('disconnect', () => {
    console.log('Socket: User Disconnected');
  });
});

http.listen(socketPort, function () {
  logger.info(`HTTP Server Running @ PORT ${socketPort}`);
});

app.listen(config.port);
