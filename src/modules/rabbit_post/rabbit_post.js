import * as skyC from "../../lib/sky_connect";
import * as utility from "../../lib/utility";
import * as  path from "path";

// Breaking News Object
const breakingNews = {
  type: "breaking_news_marquee",
  action: "save",
  entry: {
    title: "Breaking News Title",
    nid: "918383",
    type: "breaking_news_marquee",
    revision_timestamp: 1544542109,
    marquee_list: [
      {
        marquee_type: "text",
        headline: "Breaking News MSG",
        headline_visible: 1,
        label: "",
        mpx_id: null,
        embed: null
      }
    ]
  },
  "messageId": "a0be5fd7-8c68-4611-a769-beb4bc3affa6",
  "user": "didarul.amin@nbcuni.com"
}

// Article Object
const articleMSG = {
  type: "news_entry",
  action: "save",
  entry: {
    presentation: "normal",
    article_type: "post",
    ads_enabled: 1,
    comments_enabled: 1,
    search_enabled: 1,
    first_publication_date: 1541018700,
    publication_date: 1541018700,
    breaking_news: 0,
    external_link: "",
    native_ad: 0,
    sponsor_label: "",
    show_on_cover: 0,
    cover_headline: "",
    source_site_name: "",
    editorial_notes: "Didarul TEST",
    video_art_url: "",
    breaking_news_alert: 0,
    alert_type: null,
    breaking_news_alert_title: "",
    breaking_news_alert_delivery: "",
    breaking_news_headline: "",
    breaking_news_alert_email_body: "",
    seo_slug: "Didarul TEST",
    social_media_headline: "Didarul TEST",
    source_type: "author",
    news_keywords: "",
    standout_article: 0,
    autoplay_video: 0,
    canonical_url_override: "",
    article_dek: "",
    sponsored_by: "",
    byline_display: "standard",
    auto_curation: true,
    hide_recommendations: false,
    linked_sections: [],
    linked_topics: [],
    linked_sub_topics: [],
    source: [],
    curated_list: [],
    content: "<p>Didarul TEST</p>",
    summary: "Didarul TEST",
    main_art: {
      fid: "2598750",
      url: "ss-180909-north-korea-anniversary-celebration-1438_37f54d2a279db233b4b62269db98440e-fit-880w.jpg",
      width: "0",
      height: "581",
      title: "",
      alt_text: "",
      caption: "",
      crop_rect: null,
      focus_rect: null,
      byline: "",
      copyright: "",
      source: "",
      use_video_art: 0
    },
    hero: {
      presentation: "default",
      rel: "nbcng:mainImage",
      name: "ncim2598750"
    },
    social_media_image: null,
    cover_image: {
      fid: null,
      url: null,
      width: "0",
      height: "0",
      title: null,
      alt_text: null,
      caption: null,
      crop_rect: null,
      focus_rect: null,
      byline: null,
      copyright: null,
      source: null,
      use_video_art: 0
    },
    tease_image: null,
    storyline: [],
    labels: [],
    channel: [],
    linked_channels: [],
    section: ["20221"],
    topic: [],
    sub_topic: [],
    primary_vertical: [],
    linked_vertical_subsections: [],
    title: "Didarul TEST",
    seo_headline: "Didarul TEST",
    nid: "824776",
    type: "news_entry",
    revision_timestamp: 1542773515,
    byline: [
      {
        byline: null,
        byline_type: "author"
      }
    ],
    hidden: 0,
    featuredAuthor: {
      firstAuthorIsFeatured: false,
      firstAuthorHasImage: false
    }
  },
  messageId: "6f631eed-25e6-421c-ae17-821d1b044a5f",
  user: "didarul.amin@nbcuni.com"
};

module.exports = function (req, res) {
  var ts = Math.round(new Date().getTime() / 1000);
  logger('Rabbit Start');
  let rabbitMSG = (req.body);
  console.log("NODE OBJECT: ", rabbitMSG["breaking_news"]);
  //let nid = utility.generateUID('news', 'article').nid;
  let nid = (rabbitMSG["rock_cms_nid"] && rabbitMSG["rock_cms_nid"] > 0) ? rabbitMSG["rock_cms_nid"] : utility.generateUID("news", "article").nid;

  let title = rabbitMSG.rock_cms_header;
  let content = rabbitMSG["tinymce-content-body"];
  articleMSG.entry.nid = nid;
  
  let main_art_fid = (rabbitMSG["main_art_fid"].length > 0) ? rabbitMSG["main_art_fid"] : 2598750;
  if (main_art_fid) {
    articleMSG.entry.main_art.fid = main_art_fid;
    articleMSG.entry.hero.name = `ncim${main_art_fid}`;
    articleMSG.entry.main_art.url = path.basename(rabbitMSG["main_art_image_url"]);
  }

  articleMSG.entry.first_publication_date = ts;
  articleMSG.entry.publication_date = ts;
  articleMSG.entry.revision_timestamp = ts;
  articleMSG.entry.title = title;
  articleMSG.entry.content = content;
  //console.log("POST Data: ", title);

  skyC.send(articleMSG);
  if (rabbitMSG["breaking_news"] == '1') {
    logger("! Sending Breaking News Alert ! ");
    breakingNews.entry.title = title;
    breakingNews.entry.revision_timestamp = ts;
    //breakingNews.entry.marquee_list[0].headline = `<p><a href="https://uat.nbcnews.com/news/africa/preview-test-n${nid}" rel="nofollow">${title}</a></p>`;
    breakingNews.entry.marquee_list[0].headline = content;
    console.log("Breaking News Data: ", breakingNews);
    skyC.send(breakingNews);
  }
  
  res.json({ status: 200, msg: "Sent Successfully", nid: nid });
  logger("Rabbit End");
  console.log("NODE ID: ", nid);
};
