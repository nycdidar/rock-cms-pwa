'use strict';

importScripts("worker-libs.js");
importScripts("dexie.js");
console.log('MSG FROM SKY WORKER', 'READY TO LISTEN YOUR COMMANDS');

/**
 * Web Worker In Action
 */
var db = new Dexie("bento-data-store");
db.version(1).stores({
  api_data: '++id'
});

const CWHEndpoint = "https://kemne.com/didar/cwhendpoint.php";
const bentoAPI = `https://staging.newsdigitalapi.com/`;

const loadNodeData = (url, aimsImage) => {
  load(`${url}`, function(xhr) {
    var result = xhr.responseText;
    let nodeObject = JSON.parse(result);
    nodeObject = nodeObject[0];
    let nid = nodeObject.entry.nid;
    console.log("Data in Worker Bee", nodeObject);
    db.api_data.add({ id: nid, image: aimsImage, nodeObject });
  });
};

const getLatestStories = (nid) => {
  let count = 0;
  let bentoNID = `ncna${nid}`;
  let bentoQuery = `?variables=%7B%0A%20%20%22type%22%3A%20%22type%3Aarticle%22%0A%7D&operationName=getDidarulArticleList&query=query%20getDidarulArticleList(%24type%3AString)%20%7B%0A%20%20search(query%3A%24type%2C%20filters%3A%22id%3A${bentoNID}%22%20)%20%7B%0A%20%20%20%20items%20%7B%0A%20%20%20%20%20%20...%20on%20Article%20%7B%0A%20%20%20%20%20%20%20%20id%0A%20%20%20%20%20%20%20%20headline%20%7B%0A%20%20%20%20%20%20%20%20%20%20primary%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20type%0A%20%20%20%20%20%20%20%20datePublished%0A%20%20%20%20%20%20%20%20publisher%20%7B%0A%20%20%20%20%20%20%20%20%20%20name%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20primaryImage%20%7B%0A%20%20%20%20%20%20%20%20%20%20id%0A%20%20%20%20%20%20%20%20%20%20url%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20short%0A%20%20%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%7D%0A%20%20%20%20%7D%0A%20%20%7D%0A%7D`;
  load(`${bentoAPI}${bentoQuery}`, function(xhr) {
    var result = xhr.responseText;
    let nodeObject = JSON.parse(result);
    let bentoDataList = nodeObject.data.search.items;
    db.api_data.delete();
    bentoDataList.forEach(function(node) {
      let aimsImage = node.primaryImage ? node.primaryImage.url.short : "";
      loadNodeData(`${CWHEndpoint}?nid=${nid}`, aimsImage);
      count++;
      if (count == 24) postMessage("Download-Completed");
    });
  });  
}

self.addEventListener('message', e => {
  let url = e.data.url;
  (async (nid) => {
    let total_rec = await db.api_data.where('id').equals(e.data.nid.toString()).count();
    console.log("DDDXXXX" + e.data.nid, total_rec);
    if (total_rec == 0) {
      getLatestStories(e.data.nid);
    }
  })();   
});
