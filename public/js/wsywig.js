/**
 * WSYWIG Configs
 */
// TinyMCE Headline Input Config
var dfreeHeaderConfig = {
  selector: '.dfree-header',
  menubar: false,
  inline: true,
  theme: 'inlite',
  selection_toolbar: false
  };
// TinyMCE Body Input Config
var dfreeBodyConfig = {
    selector: '.dfree-body',
    menubar: true,
    inline: true,
    theme: 'inlite',
    plugins: [
    'contextmenu',
    'link',
    'autosave',
    'textcolor',
    'rock_media'
  ],
  toolbar: [
    'undo redo | bold italic underline | fontselect fontsizeselect | restoredraft | forecolor',
    'forecolor backcolor | alignleft aligncenter alignright alignfull | link unlink | numlist bullist outdent indent'
  ],
  insert_toolbar: 'h1 h2 h3 | fontselect fontsizeselect | forecolor | blockquote | rock_media',
  selection_toolbar: ' bold italic | h1 h2 h3 | fontselect fontsizeselect | forecolor | alignleft aligncenter alignright alignjustify  | numlist bullist | blockquote',
  autosave_interval: "5s",
  contextmenu: "rock_media link",
  content_css: "/css/content.css"
};

tinymce.init(dfreeHeaderConfig);
tinymce.init(dfreeBodyConfig);
