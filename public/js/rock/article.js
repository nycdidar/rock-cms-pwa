window.addEventListener('DOMContentLoaded', function () {
  var avatar = document.getElementById('avatar');
  var image = document.getElementById('image');
  var input = document.getElementById('input');
  var $progress = $('.progress');
  var $progressBar = $('.progress-bar');
  var $alert = $('.alert');
  var $modal = $('#modal');
  var cropper;

  $('[data-toggle="tooltip"]').tooltip();

  input.addEventListener('change', function (e) {
    var files = e.target.files;
    var done = function (url) {
      input.value = '';
      image.src = url;
      $alert.hide();
      $modal.modal('show');
    };
    var reader;
    var file;
    var url;

    if (files && files.length > 0) {
      file = files[0];

      if (URL) {
        done(URL.createObjectURL(file));
      } else if (FileReader) {
        reader = new FileReader();
        reader.onload = function (e) {
          done(reader.result);
        };
        reader.readAsDataURL(file);
      }
    }
  });

  $modal.on('shown.bs.modal', function () {
    cropper = new Cropper(image, {
      aspectRatio: 16 / 9,
      viewMode: 3,
    });
  }).on('hidden.bs.modal', function () {
    cropper.destroy();
    cropper = null;
  });

  document.getElementById('crop').addEventListener('click', function () {
    var initialAvatarURL;
    var canvas;

    $modal.modal('hide');
    var imageData = '';
    if (cropper) {
      canvas = cropper.getCroppedCanvas({
        width: 360,
        height: 360,
      });
      initialAvatarURL = avatar.src;
      imageData = canvas.toDataURL();
      avatar.src = imageData;
      sky_log("Image Data", imageData);
      $progress.show();
      $alert.removeClass('alert-success alert-warning');
      canvas.toBlob(function (blob) {
        var formData = new FormData();
        formData.append('avatar', blob, 'avatar.jpg');
        //http://kemne.com/didar/test.jpg
        $.ajax({
          type: "POST",
          url: "http://kemne.com/didar/php-file-upload.php",
          data: {
            image_data: imageData,
            file_name: "test.jpg"
          },
          success: function (output) {

            $.ajax({
              type: "POST",
              url: "/aimsify-image",
              success: function(output) {
                sky_log("AIMS Data", output);
                $("#main_art_fid").val(output.fid);
                $("#main_art_image_url").val(`https://sys02-media.s-nbcnews.com/i/${output.imageURL}`);
              }
            });

          }
        });

      });
    }
  });
});


// Auto Hide SEO Containers
window.addEventListener('DOMContentLoaded', function () {
  $("#seo_container").toggle();
  $("#rock_cms_header").click(function () {
    $("#seo_container").toggle("slow", function () {
    });
  });
});