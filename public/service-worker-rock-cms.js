const CACHE_NAME = 'static-cache';
const OFFLINE_URL = '/offline.html';

importScripts('https://storage.googleapis.com/workbox-cdn/releases/3.4.1/workbox-sw.js');
// Force production builds
//workbox.setConfig({ debug: false });

if (workbox) {
  console.log(`Yay! Workbox is loaded 🎉`);
} else {
  console.log(`Boo! Workbox didn't load 😬`);
}

// App Shell Files. List of files needs to be precached for offline experience
var urlsToCache = [
  ".",
  "./index.html",
  "./offline.html",
  "./css/bootstrap.min.css",
  "./css/bootstrap.min.css.map",
  "./css/dashboard.css",
  "./js/cropper/cropper.css",
  "./js/jquery-3.3.1.min.js",
  "./js/dexie.js",
  "./js/wsywig.js",
  "./js/popper.min.js",
  "./js/bootstrap.min.js",
  "./js/cropper/cropper.js",
  "./assets/ajax-loader.gif",
  "./assets/content-data-image-header.jpg",
  "./assets/barack-obama.jpg",
  "./assets/donald-trump.jpg",
  "./assets/nbcnews.png",
  "./assets/404.gif",
  "./assets/rock-icon.png",
  "./assets/no_image_main_art.jpg",
  "./js/skins/lightgray/fonts/tinymce-small.svg",
  "./js/skins/lightgray/fonts/tinymce-small.ttf",
  "./js/skins/lightgray/fonts/tinymce.svg",
  "./js/skins/lightgray/fonts/tinymce.ttf",
  "./js/skins/lightgray/fonts/tinymce.woff",
  "./js/tinymce.min.js",
  "./js/skins/lightgray/skin.min.css",
  "./js/skins/lightgray/content.inline.min.css",
  "./js/themes/modern/theme.min.js",
  "./js/themes/inlite/theme.min.js",
  "./js/plugins/link/plugin.min.js",
  "./js/plugins/autosave/plugin.min.js",
  "./js/plugins/colorpicker/plugin.min.js",
  "./js/plugins/contextmenu/plugin.min.js",
  "./js/rock/sky_library.js",
  "/media"
];

// Workbox Precache
workbox.precaching.precache(urlsToCache);
workbox.precaching.precacheAndRoute(urlsToCache);

workbox.routing.registerRoute(
  // Cache CSS files
  /.*\.css/,
  // Use cache but update in the background ASAP
  workbox.strategies.cacheFirst({
    // Use a custom cache name
    cacheName: CACHE_NAME,
  })
);

workbox.routing.registerRoute(
  // Cache JS files
  /.*\.js/,
  // Use cache but update in the background ASAP
  workbox.strategies.cacheFirst({
    // Use a custom cache name
    cacheName: CACHE_NAME,
  })
);

workbox.routing.registerRoute(
  // Cache image files
  /.*\.(?:png|jpg|jpeg|svg|gif)/,
  // Use the cache if it's available
  workbox.strategies.cacheFirst({
    // Use a custom cache name
    cacheName: CACHE_NAME,
    plugins: [
      new workbox.expiration.Plugin({
        // Cache only 200 images
        maxEntries: 200,
        // Cache for a maximum of a week
        maxAgeSeconds: 7 * 24 * 60 * 60,
      })
    ],
  })
);

const articleHandler = ({url, event, params}) => {
   // Response will be “A guide on Workbox”
  return new Response(
    `<h1> This is an example of custom routing using WorkBox plugin by Google !!! </h1>`,
    { headers: {'Content-Type': 'text/html'}}
  );
};

workbox.routing.registerRoute(
  /\/article\/.+/,
  articleHandler
);

const productHandler = ({url, event, params}) => {
  // Response will be “A guide on Workbox”
 return new Response(
   `{"name": "Amazon Fire TV","Price": "$123.33"}`,
   { headers: {'Content-Type': 'application/json'}}
 );
};

workbox.routing.registerRoute(
 /\/product\/.+/,
 productHandler
);

/*
workbox.routing.registerRoute(
  "/post/11111",
  async ({ url, event, params }) => {
    const clients = self.clients.matchAll();
    for (const client of clients) {
      client.postMessage("HELLOOOO *** MSG FROM SERVICE WORKER");
    }  
  }
);
*/

// Dash Board API DATA
workbox.routing.registerRoute(
  "/api/dash/dashdata",
  workbox.strategies.networkFirst({
    cacheName: CACHE_NAME,
    plugins: [
      new workbox.expiration.Plugin({
        //Force cache clear on every two hours.
        maxAgeSeconds: 2 * 60 * 60
      })
    ]
  })
);

// Media Modal Data
workbox.routing.registerRoute(
  "/media",
  workbox.strategies.networkFirst({
    cacheName: CACHE_NAME,
    plugins: [
      new workbox.expiration.Plugin({
        //Force cache clear on every two hours.
        maxAgeSeconds: 2 * 60 * 60
      })
    ]
  })
);

// Global Search Data
workbox.routing.registerRoute(
  "/search/advanced",
  workbox.strategies.networkFirst({
    cacheName: CACHE_NAME,
    plugins: [
      new workbox.expiration.Plugin({
        //Force cache clear on every two hours.
        maxAgeSeconds: 2 * 60 * 60
      })
    ]
  })
);



const queue = new workbox.backgroundSync.Queue('sky_connect_queue');
workbox.routing.registerRoute(
  RegExp("/submit-article-data"),
  workbox.strategies.networkOnly({
    plugins: [
      {
        fetchDidFail: async ({ request }) => {
          await queue.addRequest(request);
        }
      }
    ]
  }),
  "POST"
);

let loopCounter = 0;
let interval = 5;
const loopTimer = () => {
  setTimeout(function () {
    if (loopCounter == interval) {
      console.log("Service Worker Background Sync");
      queue.replayRequests();
      loopCounter = 0;
    }
    loopTimer();
    loopCounter++;
  }, 1000);
};


self.addEventListener("message", event => {
  console.log("=====================================================================");
  console.log("========================BACKGROUND STUFF ============================");
  console.log(event.data);
  console.log("=====================================================================");
  if (event.data === "replayRequests") {
    console.log("========NOW I WILL REPLY THE MSG============================");
    loopTimer();
    //queue.replayRequests();
  }
});

self.addEventListener('install', function(event) {
  event.waitUntil(
    caches.open(CACHE_NAME)
    .then(function(cache) {
      return cache.addAll(urlsToCache);
    })
  );
});


addEventListener('fetch', event => {
  let urlParams = event.request.url.split("http://localhost:3000/");
  let Param = (urlParams[1]) ? urlParams[1] : false;
  var matches = Param.match(/login|submit-article-data|aimsify-image/g);
  //if (matches) console.log("URL PARAMS:", matches[0]);
  if (!matches) {
    event.respondWith((async function () {
      console.log("Route Request:", event.request.url);
      const cachedResponse = await caches.match(event.request);
      return cachedResponse || fetchAndCache(event.request);
    })());
  }
});

function fetchAndCache(url) {
  return fetch(url)
  .then(function(response) {
    console.log("**** RUNNING OFFLINE ******");
    // Check if we received a valid response
    if (!response.ok) {
      throw Error(response.statusText);
    }
    return caches.open(CACHE_NAME)
    .then(function(cache) {
      cache.put(url, response.clone());
      return response;
    });
  })
  .catch(function(error) {
    console.log('Request failed:', error);
    console.log('Request failed:', OFFLINE_URL);
    return fetch(OFFLINE_URL);
  });
}
