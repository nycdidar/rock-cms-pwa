
import * as form from "../../lib/form_api";
import * as skyC from "../../lib/sky_connect.js";
import * as util from "../../lib/utility";
module.exports = async function (req, res) {
  const currentTime = Math.floor(Date.now() / 1000);
  logger("M at Singe Page APP");
  // logger(util.generateUID().fid);

  const postID = req.params.postID ? req.params.postID : false;
  let sessionObj = (req.session.userData) ? req.session.userData : {};

  let storeSessionInfo = `
    if (typeof(Storage) !== "undefined") {
      localStorage.setItem("sky_connect_data", "{uid: ${sessionObj.uid},name:${sessionObj.name},email:${sessionObj.user_name}}");
    }
  `;

  let headerJS = `
  window.cms_one_uid = ${sessionObj.uid};
  `;


  //util.exportJS(headerJS, "main_header_1234.js");

  //console.log("KKKKKKKKK", req);
  //console.log(req.body.rock_cms_header);

  // Post action/message
  let displayMSG = "** Welcome To Rock CMS X ** ";
  if (req.body.rock_cms_header) {
    displayMSG = "Welcome To Rock CMS PWA";
  }

  //let nodeObject = await skyC.data({ nid: postID }, "contents");
  
  //console.log("Page DOCUMENT: ", nodeObject);

  //let nodeObject = { content: '', image: '', entry: {title: '', content: ''}, title: ''};
  //let content = '';

  //nodeObject = await transform.data(nodeObject);
  //console.log("DDDD", nodeObject.entry.content);
 
  // Remove all image tags as they generate 404 and throws error of undefined because WSYWIG renders all images.
  //let content = nodeObject.entry.content.replace(/<img[^>]*>/g, "");
  //let content = nodeObject.entry.content;


  //var yourRegex = /<img\s+[^>]*?src=("|')([^"']+)\1/g;
  //var yourRegex = /\<img.+src=[\"|\'](.+?)[\"|\']/g;
  //var src = yourRegex.exec(content);
  //console.log(src);


  const formObj = {
    submitURL: "/hello",
    contentBody: "Enter Content..",
    seo: "SEO Title",
    title: form.inputTextAreaField({ title: "Enter Title", helpText: "hellsfasas", validation: false }),
    msg: form.displayMSG({
      msg: displayMSG
    }), shell_page: form.inputCheckBoxField({
      title: "Shell Page",
      value: true,
      helpText: "Shell Page",
      validation: false
    }), native_ad: form.inputCheckBoxField({
      title: "Native Ad",
      value: true,
      helpText: "Native Ad",
      validation: false
    }), hide_recommendation: form.inputCheckBoxField({
      title: "Hide Recommendation",
      value: true,
      helpText: "This will hide from feed.",
      validation: false
    }), e_commerce: form.inputCheckBoxField({
      title: "Enable E Commerce",
      value: true,
      helpText: "Enable Commerce.",
      validation: false
    }), input: form.inputTextField({
      title: "Auto Complete AJAX",
      name: "widget_product_id",
      value: "Amazon",
      id: "widget_product_id",
      ajax: true,
      remote_url: "/data/product.json",
      class: "widget_product-id",
      validation: false
    }), multivalue: form.inputTextField({
      title: "Multi value ",
      name: "multi_value_field",
      value: "",
      id: "tags",
      multi: true,
      validation: false
    }), list: form.inputListField({
      title: "Multi Select Dropdown",
      name: "test_dhjhhn2",
      multiselect: true,
      value: "6",
      list_values: "././public/data/data.json",
      id: "my_headline",
      class: "my_headline",
      validation: false
    }), sections: form.inputListField({
      title: "Sections",
      name: "sections",
      multiselect: true,
      value: "6",
      list_values: "././public/data/news-sections.json",
      id: "rock_sections",
      class: "rock_sections",
      validation: true
    }), primary_channel: form.inputListField({
      title: "Primary Channel",
      name: "primary_channel",
      multiselect: true,
      value: "6",
      list_values: "././public/data/news-primary-channel.json",
      id: "rock_primary_channel",
      class: "rock_primary_channel",
      validation: true
    }), bylines: form.inputListField({
      title: "Byline",
      name: "byline",
      multiselect: true,
      value: "6",
      list_values: "././public/data/news-bylines.json",
      id: "rock_byline",
      class: "rock_byline",
      validation: true
    }), topic: form.inputListField({
      title: "Topic",
      name: "topic",
      multiselect: true,
      value: "6",
      list_values: "././public/data/news-topic.json",
      id: "rock_topic",
      class: "rock_topic",
      validation: true
    }), sub_topic: form.inputListField({
      title: "Sub Topic",
      name: "subtopic",
      multiselect: true,
      value: "6",
      list_values: "././public/data/news-sub-topic.json",
      id: "rock_subtopic",
      class: "rock_subtopic",
      validation: true
    }), sources: form.inputListField({
      title: "Sources",
      name: "sources",
      multiselect: true,
      value: "6",
      list_values: "././public/data/news-source.json",
      id: "rock_sources",
      class: "rock_sources",
      validation: true
    }), storylines: form.inputListField({
      title: "Storylines",
      name: "storylines",
      multiselect: true,
      value: "6",
      list_values: "././public/data/news-storylines.json",
      id: "rock_storylines",
      class: "rock_storylines",
      validation: true
    }) };

let registerSkyWorker = `
  if (window.Worker && navigator.onLine) {
    console.log("Web Worker SUPPORT: YES");
    let rockWorker = new Worker("/js/sky_worker_doc.js");
    rockWorker.postMessage({ action: '', msg: 'YOU DIDARUDL', url: '', nid: '${postID}' });
    rockWorker.onmessage = function(event) {
      console.log("RECEIVED MSG FROM SKY WORKER", event.data);
    };
  }
`;


  // -- Global JS Footer -----
  const reqObj = {
    script_js: storeSessionInfo,
    script_jquery: `window.nodeID = ${postID};`,
    formJSHeader: form.formJSHeader(),
    common_js: registerSkyWorker + form.commonJS(),
    inject_script_after: '',
    header_js: headerJS,
    document_id: postID
  }
  form.resetHeader();
  // -- Global JS Footer -----

  res.render("pwa", {
    formObj, reqObj
  });

};
