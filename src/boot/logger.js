/**
 * RockCMS Logger
 */
const { transports, format, createLogger } = require("winston");
const logPath = process.env.log_file_name || 'log.log';
const level = process.env.LOG_LEVEL || "debug";

var options = {
  file: {
    level: "info",
    //filename: `${appRoot}/logs/app.log`,
    filename: logPath,
    handleExceptions: true,
    json: true,
    maxsize: 5242880, // 5MB
    maxFiles: 5,
    colorize: false,
    format: format.combine(
      format.timestamp(),
      format.colorize(),
      format.printf(
        info => `[${info.timestamp}] ${info.level}: ${info.message}`
      )
    )
  },
  console: {
    level: "debug",
    handleExceptions: true,
    json: false,
    colorize: true
  }
};

const logger = createLogger({
  format: format.combine(
    format.timestamp(),
    format.colorize(),
    format.printf(info => `[${info.timestamp}] ${info.level}: ${info.message}`)
  ),
  transports: [
    new transports.Console(options.console),
    new transports.File(options.file)
  ]
});

//logger.info("Hello again distributed logs");

module.exports = logger;
