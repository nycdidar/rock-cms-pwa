
/**
 * Global TOP Search
 */

$("#global_site_search").autocomplete({
  source: function(request, response) {
    $.ajax({
      url: "/search/advanced",
      data: {
        term: request.term
      },
      success: function(data) {
        response(data);
      }
    });
  },
  minLength: 1,
  select: function(event, ui) {
    //log("Selected: " + ui.item.value + " aka " + ui.item.id);
  }
});

$("#exampleModal").on("show.bs.modal", function (event) {
  var button = $(event.relatedTarget); // Button that triggered the modal
  var recipient = button.data("whatever"); // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this);
  modal.find(".modal-title").text("New message to " + recipient);
  modal.find(".modal-body input").val(recipient);
});

$("#rock_media_modal").on("show.bs.modal", function (event) {
  var otherInfo = $(event.relatedTarget);
  var sourceTrigger = otherInfo.data("source");

  $.ajax({
    url: "/media",
    crossDomain: true
  }).done(function(data) {
    $("#search_media_image_content").html(data.imageData.html);
    $("#search_media_video_content").html(data.videoData.html);

    $('.img-thumbnail').click(function (e) {
      let modalSelectedImage = $(this).attr("src");
      let fid = $(this).attr("fid") ? $(this).attr("fid") : 0;
      $("#main_art_fid").val(fid);
      $("#main_art_image_url").val(modalSelectedImage);
      if (sourceTrigger == 'main_art') {
        $(".main_art_image").attr("src", modalSelectedImage);
      } else {
        tinymce.activeEditor.selection.setNode(tinymce.activeEditor.dom.create('img', { src: modalSelectedImage, title: 'HELLOOO' }));
      }

      $("#rock_media_modal").click();
    });


  });
});

// Example starter JavaScript for disabling form submissions if there are invalid fields
window.addEventListener("DOMContentLoaded", function() {
  let rockFormValidation = false;
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName("needs-validation");
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener("submit", function(event) {
        event.preventDefault();
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        } else {
          rockFormValidation = true;
          postData(form);
        }
        form.classList.add("was-validated");
        }, false);
    });
  }, false);

$(".rock_preview").click(function () {
  let dataNID = ($(".rock_preview").attr("data-nid").length > 0) ? $(".rock_preview").attr("data-nid") : rock_current_post_id;
  //window.open(`https://uat.nbcnews.com/know-your-value/nbc-asian-america-making-of-an-olympian/test-ncna${dataNID}`, "_blank");

  if ($("#breaking_news").val() == '1') {
    window.open(`https://uat.nbcnews.com`, "_blank");
  } else {
    window.open(`https://uat.nbcnews.com/news/africa/preview-test-n${dataNID}`, "_blank");
  }
  
});