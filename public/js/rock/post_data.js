/**
 * Clinet Side Data Submission
 */

const formSubmitHandler = form => {
  $(".sky_auto_save").each(function () {
    let fieldName = $(this).attr("data_field");
    let fieldValue = $(this).val();
    articleMSG.entry.nid = rock_current_post_id;
    if (fieldName) {
      articleMSG.entry[fieldName] = fieldValue;
    }
  });

};

const finalMSG = () => {
  sky_log("Final Output: " + rock_current_post_id, articleMSG);
  return articleMSG;
}

const submitRequest = () => {
  var data = $("form").serialize();
  $.ajax({
    url: "/submit-article-data",
    context: document.body,
    method: "POST",
    data: data,
  }).done(function (data) {
    sky_log("Received Data", data);
    $(".rock_preview").attr("data-nid", data.nid);
    //alert(`Published Successfully. Node ID: ${data.nid}`);
    const cacheBust = Math.floor(Math.random() * (900000000 - 100000000 + 1)) + 100000000;
    //window.open(`https://uat.nbcnews.com/know-your-value/nbc-asian-america-making-of-an-olympian/test-ncna${data.nid}?${cacheBust}`, "_blank");

    if ($("#breaking_news").val() == '1') {
      window.open(`https://uat.nbcnews.com?${cacheBust}`, "_blank");
    } else {
      window.open(`https://uat.nbcnews.com/news/africa/preview-test-n${data.nid}?${cacheBust}`, "_blank");
    }

  }).fail(function() {
    alert("Failed to submit, NO WORRIES !!! it will queue up and send automatically when server comes back online..");
    
    navigator.serviceWorker.ready.then(function (reg) {
      // set up a message channel to communicate with the SW
      var channel = new MessageChannel();
      channel.port1.onmessage = function (e) {
        console.log(e);
        handleChannelMessage(e.data);
      }
      mySW = reg.active;
      mySW.postMessage('replayRequests', [channel.port2]);
    });



  });
 
};

async function postData(form) {
  let processFormData = await formSubmitHandler(form);
  let msgData = await finalMSG();
  let pushData = await submitRequest();
  
}


/**
 * Article MSG Contract
 */
const articleMSG = {
  "type": "news_entry",
  "action": "save",
  "entry": {
    "presentation": "normal",
    "article_type": "post",
    "ads_enabled": 1,
    "comments_enabled": 1,
    "search_enabled": 1,
    "first_publication_date": 1541018700,
    "publication_date": 1541018700,
    "breaking_news": 0,
    "external_link": "",
    "native_ad": 0,
    "sponsor_label": "",
    "show_on_cover": 0,
    "cover_headline": "",
    "source_site_name": "",
    "editorial_notes": "Didarul TEST",
    "video_art_url": "",
    "breaking_news_alert": 0,
    "alert_type": null,
    "breaking_news_alert_title": "",
    "breaking_news_alert_delivery": "",
    "breaking_news_headline": "",
    "breaking_news_alert_email_body": "",
    "seo_slug": "Didarul TEST",
    "social_media_headline": "Didarul TEST",
    "source_type": "author",
    "news_keywords": "",
    "standout_article": 0,
    "autoplay_video": 0,
    "canonical_url_override": "",
    "article_dek": "",
    "sponsored_by": "",
    "byline_display": "standard",
    "auto_curation": true,
    "hide_recommendations": false,
    "linked_sections": [],
    "linked_topics": [],
    "linked_sub_topics": [],
    "source": [],
    "curated_list": [],
    "content": "<p>Didarul TEST</p>",
    "summary": "Didarul TEST",
    "hero": {
      "presentation": "noImage"
    },
    "main_art": null,
    "social_media_image": null,
    "cover_image": {
      "fid": null,
      "url": null,
      "width": "0",
      "height": "0",
      "title": null,
      "alt_text": null,
      "caption": null,
      "crop_rect": null,
      "focus_rect": null,
      "byline": null,
      "copyright": null,
      "source": null,
      "use_video_art": 0
    },
    "tease_image": null,
    "storyline": [],
    "labels": [],
    "channel": [],
    "linked_channels": [],
    "section": [
      "20221"
    ],
    "topic": [],
    "sub_topic": [],
    "primary_vertical": [],
    "linked_vertical_subsections": [],
    "title": "Didarul TEST",
    "seo_headline": "Didarul TEST",
    "nid": "929381",
    "type": "news_entry",
    "revision_timestamp": 1542773515,
    "byline": [
      {
        "byline": null,
        "byline_type": "author"
      }
    ],
    "hidden": 0,
    "featuredAuthor": {
      "firstAuthorIsFeatured": false,
      "firstAuthorHasImage": false
    }
  },
  "messageId": "6f631eed-25e6-421c-ae17-821d1b044a5f",
  "user": "didarul.amin@nbcuni.com"
};