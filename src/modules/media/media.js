/**
 * Media Endpoint
 */
import fs from "fs";
import * as BentoAPI from "../../lib/bento_api";

const endpoint = "https://staging.newsdigitalapi.com";

module.exports = async function(req, res) {
  const currentTime = Math.floor(Date.now() / 1000);
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  res.header("Cache-Control", "private, no-cache, no-store, must-revalidate");
  res.header("Expires", "-1");
  res.header("Pragma", "no-cache");

  let imageBlocks = "";
  let videoBlocks = "";

  const imageList = await JSON.parse(fs.readFileSync("././public/data/media.json", "utf8"));
  Object.keys(imageList).forEach(key => {
    imageBlocks += `
          <a href="#">
            <figure>
              <img src="${imageList[key].filename}" alt="${imageList[key].alt}" title = "${imageList[key].title}" class = "rock_media_image img-thumbnail" fid = "${imageList[key].fid}">
              <figcaption>
                ${imageList[key].title}
              </figcaption>
            </figure>
          </a>
          `;
  });
  let imagePagination = "";
  if (imageList.length > 1) {
    for (let i = 1; i <= 10; i++) {
      const activeTab = i === 1 ? "active" : "";
      imagePagination += `<li class="paginate_button ${activeTab}"><a href="#" aria-controls="example2" data-dt-idx="${i}" tabindex="0">${i}</a></li>`;
    }
  }

  const imageData = { html: imageBlocks, pageInfo: imagePagination };

  const videos = await BentoAPI.video(endpoint, {
    op: "videoSearch",
    filters: { publisher: "nbcnews", type: "video" }
  });
  if (videos.search.items.length > 0) {
    Object.keys(videos.search.items).forEach(key => {
      videoBlocks += `
            <a href="#">
              <figure>
                <img src="${videos.search.items[key].teaseImage.url.primary}" alt="${videos.search.items[key].description.primary}" class = "rock_media_video img-thumbnail">
                <figcaption>
                  ${videos.search.items[key].headline.primary}
                </figcaption>
              </figure>
            </a>
            `;
    });
  }
  let videoPagination = "";
  if (videos.search.pagination.totalPages > 1) {
    for (let i = 1; i <= videos.search.pagination.totalPages; i++) {
      const activeTab = i == videos.search.pagination.page ? "active" : "";
      videoPagination += `<li class="paginate_button ${activeTab}"><a href="#" aria-controls="example2" data-dt-idx="${i}" tabindex="0">${i}</a></li>`;
    }
  }
  const videoData = { html: videoBlocks, pageInfo: videoPagination };

  res.send({ imageData, videoData });
};
