import fs from "fs";

module.exports = async function(req, res) {
  const currentTime = Math.floor(Date.now() / 1000);
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Cache-Control", "private, no-cache, no-store, must-revalidate");
  res.header("Expires", "-1");
  res.header("Pragma", "no-cache");

  let JSData = fs.readFileSync(`././src/modules/system/${req.params.jsfile}`, "utf8");
  if (req.params.jsfile == 'footer.js') {
    // Dynamically Change Script
    JSData = JSData.replace('Trump', 'Not doing great');
  }

  res.type(".js");
  res.send(JSData);
};
