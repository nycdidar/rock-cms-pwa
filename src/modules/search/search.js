import fs from "fs";

module.exports = async function (req, res) {
  const currentTime = Math.floor(Date.now() / 1000);
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
  res.header('Expires', '-1');
  res.header('Pragma', 'no-cache');

  logger("Loading Search API Data");
  const searchData = JSON.parse(fs.readFileSync("././public/data/site_search.json", "utf8"));
  res.json(searchData);
};
