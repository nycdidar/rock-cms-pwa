// Sky Connect Module
import * as config_settings from '../config';
import * as mysql from 'mysql';
import { MongoClient } from 'mongodb';
import * as amqp from "amqplib/callback_api";
import fs from 'fs';
var settings = {};
var showLog = true;
var dbCon;
let dbStatus = false;

export async function list(query, table = 'contents') {
  return new Promise(resolve => {
    MongoClient.connect(config_settings.mongo_conn_str, { useNewUrlParser: true }, function (err, db) {
      if (err) {
        console.log('Error occurred while connecting to MongoDB Atlas...\n', err);
        return false;
      } 
      const dbo = db.db(config_settings.mongo_db);
      dbo.collection(table).find({}).toArray(function (err, result) {
        if (err) throw err;
        resolve(result);
      });
    });
  });
}    
    

export async function data(query, table = 'users') {
  return new Promise(resolve => {
    MongoClient.connect(config_settings.mongo_conn_str, { useNewUrlParser: true }, function(err, db) {
        if (err) {
          console.log("Error occurred while connecting to MongoDB Atlas...\n", err);
        }
        const dbo = db.db(config_settings.mongo_db);
        dbo.collection(table).findOne(query, function (err, result) {
          if (err) throw err;
          resolve(result);
          db.close();
        });
      
      });
  });
}

export async function insert(query, table = 'users') {
  return new Promise(resolve => {
    MongoClient.connect(config_settings.mongo_conn_str, { useNewUrlParser: true }, function (err, db) {
      if (err) {
        console.log('Error occurred while connecting to MongoDB Atlas...\n', err);
      }
      const dbo = db.db(config.mongo_db);
      dbo.collection("users").insertMany(query, function(err, res) {
        if (err) throw err;
        db.close();
      });
    });
  });
}

export async function update(query, updateQuery, table = 'users') {
  return new Promise(resolve => {
    MongoClient.connect(config_settings.mongo_conn_str, { useNewUrlParser: true }, function (err, db) {
      if (err) {
        console.log('Error occurred while connecting to MongoDB Atlas...\n', err);
      }
      const dbo = db.db(config_settings.mongo_db);
      var newvalues = { $set: updateQuery };
      dbo.collection(table).updateOne(query, newvalues, function (err, res) {
        if (err) throw err;
        //console.log("1 document updated");
        resolve(true);
      });
      db.close();
    });
  });
}

export function config(config) {
  return new Promise(function (resolve, reject) {
    const config = require("../config");
    settings = config;
    dbCon = mysql.createConnection(settings.db_settings);
    dbCon.connect(function (err) {
      if (err) {
        //throw err;
        console.log("Could NOT connect!");
        dbStatus = false;
        resolve(false);
      } else {
        console.log("Database Connected!");
        dbStatus = true;
        resolve(true)
      }
    });
  });
} 

export function getData (query, callback) {
  var dbData;
  dbCon.query(query, function (err, result) {
    if (err) throw err;
    //console.log(result);
    if (callback && typeof (callback) == "function") return callback(result);
    return result;
  });
}

export function getDataC(query) {
  return new Promise(resolve => {
    var dbData;
    console.log(query);
    dbCon.query(query, function (err, result) {
      if (err) throw err;
      resolve(result);
    });
  });
}

export function fetchObj(query, callback) {
  var dbData;
  dbCon.query(query, function (err, result) {
    if (err) throw err;
    let rowObj = {};
    Object.keys(result).forEach(key => {
      rowObj[key] = result[key];
    });
    if (callback && typeof (callback) == "function") return callback(rowObj[0]);
    return result;
  });
}

export async function getNodeDetails(nid) {
  const dbStatus = await this.config(config);
  return new Promise(function (resolve, reject) {
    if (dbStatus) {
      dbCon.query(
        `SELECT * FROM contents WHERE nid = '${nid}'`,
        function (err, rows) {
          if (rows === undefined) {
            reject(new Error("Error rows is undefined"));
          } else {
            let rowObj = {};
            Object.keys(rows).forEach(key => {
              rowObj[key] = rows[key];
            });
            resolve(rowObj[0]);
          }
        }
      )
    }
    else {
      console.log("*** DB Offline: Loading from secondary source. ***");
      resolve(JSON.parse(fs.readFileSync("././public/data/article.json", "utf8")));
    }
  }
  );
}

export async function getLockedList(currentUID) {
  return new Promise(function (resolve, reject) {
    let UIDQuery = (currentUID) ? ` AND locked_by != '${currentUID}'` : '';
    dbCon.query(
      `SELECT nid, locked_by FROM contents WHERE locked = '1' ${UIDQuery}`,
      function (err, rows) {
        if (rows === undefined) {
          reject(new Error("Error rows is undefined"));
        } else {
          let rowObj = {};
          Object.keys(rows).forEach(key => {
            Object.keys(rows[key]).forEach(keySub => {
              rowObj[rows[key].nid] = rows[key][keySub];
            })
          });
          resolve(rowObj);
        }
      }
    )
  }
  );
}


export async function doc(nid) {
  let rowData = await this.getNodeDetails(nid);
  //rowData.title = `PROCESSED: ${rowData.title}`;
  return rowData;
}

export function displayData (query, res, callback) {
  result = this.getData(query, callback);
  res.send(getData(query, callback));
  return;
}

/**
 * Send to Rabbit
 * @param {*} jsonData 
 * @param {*} callback 
 */
export function send(jsonData, type = 'content', callback) {
  amqp.connect(config_settings.rabbit_prod, function (err, conn) {
    conn.createChannel(function (err, ch) {
      var q = config_settings.rabbit_queue;
      var msg = JSON.stringify(jsonData);
      //ch.assertQueue(q, { durable: true });
      //ch.sendToQueue(q, new Buffer(msg));
      let exchangeType = config_settings.rabbit_exchange;
      if (type == "file") exchangeType = config_settings.rabbit_file_exchange;
      var ok = ch.assertExchange(exchangeType, config_settings.rabbit_type, {durable: true});
      ch.publish(exchangeType, config_settings.rabbit_key, Buffer.from(msg));
      if (callback && typeof (callback) == "function") return callback();
      console.log("Message Sent");
      //conn.close();
      //console.log(" [x] Sent '%s'", msg);
      //setTimeout(function() { conn.close(); }, 500);
    });
  });
}

