
import * as form from "../../lib/form_api";
import * as skyC from "../../lib/sky_connect.js";
import * as crypto from "crypto";
module.exports = async function (req, res) {
  const currentTime = Math.floor(Date.now() / 1000);

  let userName = req.body.user_name ? req.body.user_name : false;
  let userData;

  if (userName) {
    let authToken = crypto.randomBytes(20).toString("hex");
    let resultUser = await skyC.data({ email: new RegExp(userName, "i") }, "users");
    let updateAuthToken = await skyC.update({ email: userName }, { name: "DidarulTEST", auth_token: authToken }, "users");

    userData = {
      name: resultUser.name,
      user_name: userName,
      role: "admin",
      uid: resultUser.uid,
      landing_page: "/post/81234"
    };

    req.session.userData = userData;

    if (userData.landing_page) {
      //res.redirect(userData.landing_page);
      res.redirect("/post/81234");
      return;
    } else {
      res.redirect("/post/81234");
      return;
    }

  }


  console.log(userData);

  // -- Global JS Footer -----
  const reqObj = {
    script_js: '',
    script_jquery: ``,
    formJSHeader: '',
    common_js: '',
    inject_script_after: '',
    document_id: ''
  }
  form.resetHeader();
  // -- Global JS Footer -----
  res.render("login", {
   
  });

};
