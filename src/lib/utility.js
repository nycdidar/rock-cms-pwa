/**
 * UTILITY FUNCTIONS
 * 
 */

import * as fs from "fs";

export function slugify(text = "") {
  return text
    .toString()
    .toLowerCase()
    .replace(/\s+/g, "-") // Replace spaces with -
    .replace(/[^\w\-]+/g, "") // Remove all non-word chars
    .replace(/\-\-+/g, "-") // Replace multiple - with single -
    .replace(/^-+/, "") // Trim - from start of text
    .replace(/-+$/, ""); // Trim - from end of text
}

export function aimsify(fullImage, flavor = ".focal-90x90") {
  if (typeof fullImage !== "undefined") {
    const splitter = "nbcnews.com/i/";
    const firstPart = fullImage.split(splitter);
    const extension = firstPart[1].substr(firstPart[1].length - 4);
    const mainFile = firstPart[1].split(extension);
    const fullAimsImage = firstPart[0] + "nbcnews.com/j/" + mainFile[0] + flavor + extension;
    return fullAimsImage;
  }
  return false;
};

export function generateUID(brand = 'news', type = 'article', uid = '1614', lastNum = 1000000, prefix = '') {
  const currentTime = Math.floor(Date.now() / 1000);
  let selectedBrand;
  let selectedType;
  const randomVal = Math.floor(Math.random() * (9000 - 1000 + 1)) + 1000;
  const randomFID = `${uid}${currentTime}${randomVal}`;

  switch (brand) {
    case "news":
      selectedBrand = "nc";
      break;
    case "today":
      selectedBrand = "td";
      break;
    case "msnbc":
      selectedBrand = "ms";
      break;
    default:
      selectedBrand = "nc";
  }

  switch (type) {
    case "article":
      selectedType = "na";
      break;
    case "taxonomy":
      selectedType = "tx";
      break;
    default:
      selectedBrand = "na";
  }
  
  return { 
    nid: `${uid}${currentTime}`,
    uid: `${selectedBrand}${selectedType}${uid}${currentTime}`,
    fid: randomFID
  }
}

/**
 * Dynamically Create JS File
 * @param {*} JSCode 
 * @param {*} filename 
 */
export function exportJS(JSCode = '', filename = "main_header_1234.js") {
  if (filename == 'header') filename = 'main_header.js';
  if (filename == "footer") filename = "main_footer.js";
  fs.writeFile(`./public/js/rock/${filename}`, JSCode, err => {
    if (err) throw err;
    console.log("JS Saved!");
  });
};


/**
 * Generate AIMS image on the fly
 * UAT MSG Viewer : https://uat2-msgviewer.nbcnewstools.net/msgsaver/nbcnews/image
 * Example Image: https://sys02-media.s-nbcnews.com/i/newscms/2018_45/488220680/lsclen5peabs.jpg
 * exchange name: newscms_qa_file
 * @param {*} imageObj 
 * @param {*} type 
 */

export function generateAIMS(imageObj = {}, type = 'save') {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  for (var i = 0; i < 12; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  const randomHash = text.toLowerCase();
  const yearWeek = '2018_45';
  const randomFID = Math.floor(Math.random() * (900000000 - 100000000 + 1)) + 100000000;
  let aimsImageTpl = {
    "type": "image",
    "action": type,
    "entry": {
      "uid": imageObj.uid,
      "fid": randomFID.toString(),
      "filename": randomHash + '-' + imageObj.filename,
      "filemime": "image/jpeg",
      "filesize": "324694",
      "revision_timestamp": 1541516867,
      "status": "1",
      "focus_rect": null,
      "crop_rect": null,
      "alt_text": imageObj.caption,
      "title": imageObj.caption,
      "byline": imageObj.caption,
      "caption": imageObj.caption,
      "copyright": null,
      "source": null,
      "width": 1920,
      "height": 1200,
      "revision_hash": randomHash,
      "drupal_path": imageObj.imageURL,
      "aims_image_name": randomHash + '.jpg',
      "aims_image_path": "newscms/" + yearWeek + "/" + randomFID + "/" + randomHash + '.jpg'
    },
    "messageId": randomHash,
    "user": imageObj.user
  }
  return aimsImageTpl;
};